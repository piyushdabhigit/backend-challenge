# Backend Coding Challenge

This project is for the backend coding challenge. It implements a small server application with basic functionalities such as authentication, role management, CRUD operations, etc.

## Getting Started

1. Clone the repository.
2. Install dependencies: `npm install`
3. Set up environment variables by creating a `.env` file based on the `.env.example` file.
4. Start the server: `npm start`

# Authentication Endpoints:
- **POST /api/auth/register**: Register a new user (admin or customer).
- **POST /api/auth/login**: Login with credentials and obtain a JWT token.

# User Management Endpoints:
- **GET /api/users**: Get all users (admin only).
- **GET /api/users/:userId**: Get user details by ID (admin only).
- **PUT /api/users/:userId**: Update user details (admin only).
- **DELETE /api/users/:userId**: Delete a user (admin only).

# Category Endpoints:
- **POST /api/categories**: Create a new category (admin only).
- **GET /api/categories**: Get all categories.
- **GET /api/categories/:categoryId**: Get category details by ID.
- **PUT /api/categories/:categoryId**: Update category details (admin only).
- **DELETE /api/categories/:categoryId**: Delete a category (admin only).

# Product Endpoints:
- **POST /api/products**: Create a new product (admin only).
- **GET /api/products**: Get all products. Get product details by ID.
- **PUT /api/products/:productId**: Update product details (admin only).
- **DELETE /api/products/:productId**: Delete a product (admin only).

## Additional Notes
- The server is built using Node.js, Express.js, MongoDB, and TypeScript.
- Ensure to set up environment variables as described in the `.env.example` file.
- Authentication middleware is applied to protected routes to ensure only authenticated users can access them.
- Validation middleware from the Express Validator library is used to validate input data.
- Role-based middleware (`isAdmin`) is used to restrict certain routes to admin users only.
- Make sure to follow the API documentation for correct usage of each endpoint.

# Scripts
- **start**: `npm-run-all -p build watch:server`
- **build**: `tsc`
- **watch:server**: `nodemon dist/app.js`
