import express from 'express';
import { getAllUsers,getUserById,updateUser,deleteUser } from '../controllers/userController';
import { authenticateUser } from '../middleware/authMiddleware';
import { check } from 'express-validator';
import { isAdmin } from '../middleware/roleMiddleware';

const router = express.Router();

// Middleware to authenticate user for protected routes
router.use(authenticateUser);

router.get('/user',isAdmin, getAllUsers);

router.get('/user/:userId',  getUserById);

router.put(
    '/user/:userId',
    [  
        check('password', 'Please enter a password with 6 or more characters').optional().isLength({ min: 6 }),
        check('email', 'Please enter a valid email').optional().isEmail()
    ], 
    updateUser
);

router.delete('/user/:userId', deleteUser);

export default router; 
