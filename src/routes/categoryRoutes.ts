import express from 'express';
import { check } from 'express-validator';
import { createCategory, getCategories, updateCategory, deleteCategory } from '../controllers/categoryController';
import { authenticateUser } from '../middleware/authMiddleware';
import { isAdmin } from '../middleware/roleMiddleware';

const router = express.Router();

router.use(authenticateUser);

router.post(
  '/categories',
  [
    check('name', 'Category name is required').notEmpty(),
    check('description', 'Description must be a string').optional().isString(),
  ],
  createCategory
);

// Route to get all categories or category details by ID
router.get('/categories',isAdmin, getCategories);

// Route to update category details by ID (admin only)
router.put(
  '/categories/:categoryId',
  [
    check('name', 'Category name is required').notEmpty(),
    check('description', 'Description must be a string').optional().isString(),
  ],
  updateCategory
);

// Route to delete a category by ID (admin only)
router.delete('/categories/:categoryId', deleteCategory);

export default router;
