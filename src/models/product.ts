import mongoose, { Document, Schema } from 'mongoose';
import { CategoryDocument } from './category';

export interface Product {
  name: string;
  description: string;
  price: number;
  category: CategoryDocument['_id'];
  upc: string;
}

export interface ProductDocument extends Product, Document {}

const productSchema = new Schema<ProductDocument>({
  name: { type: String, required: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
  upc: { type: String, unique: true } // Unique UPC
}, { timestamps: true });

const ProductModel = mongoose.model<ProductDocument>('Product', productSchema);

export default ProductModel;
