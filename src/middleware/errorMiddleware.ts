import { Request, Response, NextFunction } from 'express';

const errorMiddleware = (
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.error(error.stack);
  res.status(500).json({ success: false, message: 'Internal server error' });
};

export default errorMiddleware;
