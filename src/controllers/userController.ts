import { Request, Response } from 'express';
import User, { UserDocument } from '../models/user';
import { validationResult } from 'express-validator';
import bcrypt from 'bcryptjs';

export const getAllUsers = async (req: Request, res: Response) => {
    try {
      // Pagination parameters
      const page: number = parseInt(req.query.page as string, 10) || 1;
      const limit: number = parseInt(req.query.limit as string, 10) || 10; // Default limit is 10
  
      // Calculate skip value
      const skip: number = (page - 1) * limit;
  
      // Fetch users with pagination
      const users: UserDocument[] = await User.find()
        .select('-password')
        .limit(limit)
        .skip(skip);
  
      // Count total number of users
      const totalUsersCount: number = await User.countDocuments();
  
      // Calculate total pages
      const totalPages: number = Math.ceil(totalUsersCount / limit);
  
      // Respond with success and data
      res.success({ users, totalPages, currentPage: page }, 'Users fetched successfully');
    } catch (error) {
      console.error(error);
      res.error('Server error');
    }
};

export const getUserById = async (req: Request, res: Response) => {
    try {
        // Check for validation errors
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
             res.error('Validation failed', 400, errors.array());
        }
         
        // Extract userId from request parameters
        const userId: string = req.params.userId;
        
        // Validate userId format
        if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
             res.error('Invalid user ID', 400);
        }
        
        // Fetch user by ID
        const user: UserDocument | null = await User.findById(userId).select('-password');

        // Check if user with the given ID exists
        if (!user) {
             res.error('User not found', 404);
        }

        // Respond with success and user data
        res.success(user);
    } catch (error) {
        console.error(error);
        res.error('Server error');
    }
};
 
export const updateUser = async (req: Request, res: Response) => {
    try {
        // Check for validation errors
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
             res.error('Validation failed', 400, errors.array());
        }

        // Extract userId from request parameters
        const userId: string = req.params.userId;

        // Validate userId format
        if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
             res.error('Invalid user ID', 400);
        }

        // Find user by ID
        const user: any = await User.findById(userId);

        // Check if user with the given ID exists
        if (!user) {
            res.error('User not found', 404);
        }

        // Check if email already exists
        if (req.body.email && req.body.email !== user.email) {
            const existingUser = await User.findOne({ email: req.body.email });
            if (existingUser) {
                 res.error('Email already exists', 400);
            }
        }

        // Update user details
        if (req.body.username) {
            user.username = req.body.username;
        }
        if (req.body.password) {
             // Hash the password
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(req.body.password, salt);
            user.password = passwordHash;
        }
        if (req.body.email) {
            user.email = req.body.email;
        }

        // Save updated user
        const updatedUser = await user.save();

        // Respond with success and updated user data
        res.success(user,"User updated successfully")
    } catch (error) {
        console.error(error);
        res.error('Server error');
    }
};

export const deleteUser = async (req: Request, res: Response)=> {
    try {
         
        // Extract userId from request parameters
        const userId: string = req.params.userId;

        // Validate userId format
        if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
             res.error('Invalid user ID', 400);
        }

        // Find user by ID
        const user: UserDocument | null = await User.findById(userId);

        // Check if user with the given ID exists
        if (!user) {
             res.error('User not found', 404);
        }

        // Delete the user
        await User.findByIdAndDelete(userId);

        // Respond with success message
        res.success({}, 'User deleted successfully');
    } catch (error) {
        console.error(error);
        res.error('Server error');
    }
};
