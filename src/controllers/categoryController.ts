import { Request, Response } from 'express';
import Category, { CategoryDocument } from '../models/category';
import { validationResult } from 'express-validator';
import mongoose from 'mongoose';

// Create a new category (admin only)
export const createCategory = async (req: Request, res: Response) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
       return res.error('Validation failed', 400, errors.array());
    }

    // Extract category data from request body
    const { name, description } = req.body;

    // Check if the category already exists
    const existingCategory: CategoryDocument | null = await Category.findOne({ name });
    if (existingCategory) {
      return res.error('Category with this name already exists', 400);
    }
    
    // Create a new category document
    const newCategoryData = { name, description };
    const newCategory: CategoryDocument = new Category(newCategoryData);

    // Save the new category to the database
    const savedCategory: CategoryDocument = await newCategory.save();

    // Respond with success and created category data
    return res.success(savedCategory, 'Category created successfully', 201);
  } catch (error) {
    console.error(error);
    return res.error('Server error', 500);
  }
};

export const getCategories = async (req: Request, res: Response) => {
  try {
    const categoryId = req.query.catid as string;

    if (categoryId) {
      // Get category details by ID
      const category = await Category.findById(categoryId);
      if (!category) {
        return res.error('Category not found', 404);
      }
      return res.success(category);
    } else {
      // Pagination parameters
      const page: number = parseInt(req.query.page as string, 10) || 1;
      const limit: number = parseInt(req.query.limit as string, 10) || 10; // Default limit is 10
      const skip: number = (page - 1) * limit;

      // Fetch categories count for total count
      const totalCategoriesCount: number = await Category.countDocuments();

      // Calculate total pages
      const totalPages: number = Math.ceil(totalCategoriesCount / limit);

      // Get all categories with pagination
      const categories = await Category.find()
        .skip(skip)
        .limit(limit);

      // Return the fetched categories along with total pages and current page
      return res.success({ categories, totalPages, currentPage: page }, 'Categories fetched successfully');
    }
  } catch (error) {
    console.error(error);
    return res.error('Server error', 500);
  }
};

// Update category details (admin only)
export const updateCategory = async (req: Request, res: Response) => {
  try {
    const categoryId = req.params.categoryId;

    // Check if the category ID is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(categoryId)) {
      return res.error('Invalid category ID', 400);
    }

    const { name, description } = req.body;

    // Check if the category exists
    const category: any = await Category.findById(categoryId);
    if (!category) {
      return res.error('Category not found', 404);
    }

    // Check if the updated category name already exists in the database
    const existingCategoryWithName = await Category.findOne({ name });
    if (existingCategoryWithName && existingCategoryWithName._id.toString() !== categoryId) {
      return res.error('Category with this name already exists', 400);
    }

    // Update category details
    category.name = name;
    category.description = description;
    await category.save();

    // Respond with success
    return res.success(category, 'Category updated successfully', 200);
  } catch (error) {
    console.error(error);
    return res.error('Server error', 500);
  }
};

// Delete a category (admin only)
export const deleteCategory = async (req: Request, res: Response)  => {
  try {
    const categoryId = req.params.categoryId;

    // Check if the category ID is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(categoryId)) {
      return res.error('Invalid category ID', 400);
    }

    // Check if the category exists
    const category = await Category.findByIdAndDelete(categoryId);
    if (!category) {
       res.error('Category not found', 404);
    }

    // Respond with success
    res.success({}, 'Category deleted successfully', 200);
  } catch (error) {
    console.error(error);
    res.error('Server error', 500);
  }
};
